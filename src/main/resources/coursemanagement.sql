CREATE DATABASE  IF NOT EXISTS `coursemanagement` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `coursemanagement`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `classTable`
--

DROP TABLE IF EXISTS `classTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classTable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deptId` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classTable`
--

LOCK TABLES `classTable` WRITE;
/*!40000 ALTER TABLE `classTable` DISABLE KEYS */;
/*!40000 ALTER TABLE `classTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courseTable`
--

DROP TABLE IF EXISTS `courseTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseTable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` varchar(45) NOT NULL,
  `credit` double NOT NULL,
  `name` varchar(45) NOT NULL,
  `weekEnd` tinyint(4) NOT NULL,
  `weekStart` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courseTable`
--

LOCK TABLES `courseTable` WRITE;
/*!40000 ALTER TABLE `courseTable` DISABLE KEYS */;
/*!40000 ALTER TABLE `courseTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courseStudentTable`
--

DROP TABLE IF EXISTS `courseStudentTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseStudentTable` (
  `sid` int(11) NOT NULL,
  `courseId` int(11) NOT NULL,
  `grade` double DEFAULT NULL,
  PRIMARY KEY (`sid`,`courseId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courseStudentTable`
--

LOCK TABLES `courseStudentTable` WRITE;
/*!40000 ALTER TABLE `courseStudentTable` DISABLE KEYS */;
/*!40000 ALTER TABLE `courseStudentTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departmentTable`
--

DROP TABLE IF EXISTS `departmentTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departmentTable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departmentTable`
--

LOCK TABLES `departmentTable` WRITE;
/*!40000 ALTER TABLE `departmentTable` DISABLE KEYS */;
/*!40000 ALTER TABLE `departmentTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studentTable`
--

DROP TABLE IF EXISTS `studentTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studentTable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `gender` tinyint(4) NOT NULL,
  `name` varchar(45) NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `sid` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `studentTable` WRITE;
/*!40000 ALTER TABLE `studentTable` DISABLE KEYS */;
/*!40000 ALTER TABLE `studentTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lecturerTable`
--

DROP TABLE IF EXISTS `lecturerTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lecturerTable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deptId` int(11) NOT NULL,
  `gender` tinyint(4) NOT NULL,
  `isAdmin` tinyint(4) NOT NULL,
  `name` varchar(45) NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `staffId` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lecturerTable`
--

LOCK TABLES `lecturerTable` WRITE;
/*!40000 ALTER TABLE `lecturerTable` DISABLE KEYS */;
INSERT INTO `lecturerTable` VALUES (1,0,0,1,'管理员','超级管理员','e10adc3949ba59abbe56e057f20f883e','admin');
/*!40000 ALTER TABLE `lecturerTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courseLecturerTable`
--

DROP TABLE IF EXISTS `courseLecturerTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseLecturerTable` (
  `staffId` int(11) NOT NULL,
  `courseId` int(11) NOT NULL,
  PRIMARY KEY (`staffId`,`courseId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courseLecturerTable`
--

LOCK TABLES `courseLecturerTable` WRITE;
/*!40000 ALTER TABLE `courseLecturerTable` DISABLE KEYS */;
/*!40000 ALTER TABLE `courseLecturerTable` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-18 18:38:50
