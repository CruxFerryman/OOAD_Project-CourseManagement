/**
 * Created by liuhe on 21/12/2016.
 */
app.controller('GlobalCtrl',function($scope,$state){
    $scope.reload = function () {
        $state.reload();
    };
    $scope.curState = function () {
        return $state.current.name;
    };
    $scope.renderTable = function () {
        $('.table').dataTable();
    };
    $scope.logout = function () {
        sessionStorage.removeItem("token");
        sessionStorage.removeItem("userID");
        sessionStorage.removeItem("role");
    }
});