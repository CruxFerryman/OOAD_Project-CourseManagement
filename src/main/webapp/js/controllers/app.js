/**
 * Created by liuhe on 21/12/2016.
*/
app.controller("AppCtrl",function ($scope,$state) {
    $scope.app = {
        name:" 面向对象程序设计之高校选课系统 "
    };
    $scope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams){
            if (toState.name!=='signin' && !sessionStorage.getItem('token')) {
                event.preventDefault();
                $state.go('signin');
            }
        })
});