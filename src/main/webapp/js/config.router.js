'use strict';

/**
 * Config for the router
 */
angular.module('app')
    .run(
        [          '$rootScope', '$state', '$stateParams',
            function ($rootScope,   $state,   $stateParams) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
            }
        ]
    )
    .config(
        [          '$stateProvider', '$urlRouterProvider',
            function ($stateProvider,   $urlRouterProvider) {

                $urlRouterProvider
                    .otherwise('/signin');
                $stateProvider
                    .state('signin', {
                        controller: 'SigninCtrl',
                        url: '/signin',
                        templateUrl: 'tpl/signin.html'
                    })
                    .state('app', {
                        controller: 'GlobalCtrl',
                        url: '/app',
                        templateUrl: 'tpl/app.html'
                    })
                    .state('app.admin', {
                        url: '/admin',
                        templateUrl: 'tpl/admin/admin.html'
                    })
                    .state('app.lecturer', {
                        url: '/lecturer',
                        templateUrl: 'tpl/lecturer/lecturer.html'
                    })
                    .state('app.student', {
                        url: '/student',
                        templateUrl: 'tpl/student/student.html'
                    })
                    .state('app.lecturer.allCourse', {
                        controller: 'LecturerAllCourseCtrl',
                        url: '/allCourse',
                        templateUrl: 'tpl/lecturer/all-course.html'
                    })
                    .state('app.lecturer.myCourse', {
                        controller: 'LecturerMyCourseCtrl',
                        url: '/myCourse',
                        templateUrl: 'tpl/lecturer/my-course.html'
                    })
                    .state('app.student.allCourse', {
                        controller: 'StudentAllCourseCtrl',
                        url: '/allCourse',
                        templateUrl: 'tpl/student/all-course.html'
                    })
                    .state('app.student.myCourse', {
                        controller: 'StudentMyCourseCtrl',
                        url: '/myCourse',
                        templateUrl: 'tpl/student/my-course.html'
                    })
                    .state('app.admin.dept', {
                        controller: 'DeptCtrl',
                        url: '/dept',
                        templateUrl: 'tpl/admin/dept.html'
                    })
                    .state('app.admin.class', {
                        controller: 'ClassCtrl',
                        url: '/class',
                        templateUrl: 'tpl/admin/class.html'
                    })
                    .state('app.admin.lecturer', {
                        controller: 'LecturerManageCtrl',
                        url: '/lecturer',
                        templateUrl: 'tpl/admin/lecturer.html'
                    })
                    .state('app.admin.student', {
                        controller: 'StudentManageCtrl',
                        url: '/student',
                        templateUrl: 'tpl/admin/student.html'
                    })
                    .state('app.admin.course', {
                        controller: 'CourseManageCtrl',
                        url: '/course',
                        templateUrl: 'tpl/admin/course.html'
                    })
                    .state('app.admin.password', {
                        controller: 'ChangePwdCtrl',
                        url: '/password',
                        templateUrl: 'tpl/common/change-pwd.html'
                    })
                    .state('app.lecturer.password', {
                        controller: 'ChangePwdCtrl',
                        url: '/password',
                        templateUrl: 'tpl/common/change-pwd.html'
                    })
                    .state('app.student.password', {
                        controller: 'ChangePwdCtrl',
                        url: '/password',
                        templateUrl: 'tpl/common/change-pwd.html'
                    })
                    .state('app.lecturer.grade', {
                        controller: 'RecordGradeCtrl',
                        url: '/recordGrade/:courseId',
                        templateUrl: 'tpl/lecturer/grade.html'
                    })
                    .state('app.student.grade', {
                        controller: 'QueryGradeCtrl',
                        url: '/queryGrade/:courseId',
                        templateUrl: 'tpl/student/grade.html'
                    })
            }
        ]
    );