package com.ooad.finalproject.coursemanagement.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by liuhe on 21/12/2016.
 */
public interface DaoSupport<T> {
    /**
     * 插入
     * @param entity
     */
    void insert(T entity);

    /**
     * 删除
     * @param id
     */
    void delete(Serializable id);

    /**
     * 更新
     * @param entity
     */
    void update(T entity);

    /**
     * 查找
     * @param id
     * @return
     */
    T findById(Serializable id);

    /**
     * 查找多个
     * @param ids
     * @return
     */
    List<T> findByIds(Integer[] ids);

    /**
     * 查找全部
     * @return
     */
    List<T> findAll();

}
