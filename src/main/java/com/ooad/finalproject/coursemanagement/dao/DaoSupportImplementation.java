package com.ooad.finalproject.coursemanagement.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collections;
import java.util.List;

/**
 * Created by liuhe on 21/12/2016.
 */
@SuppressWarnings("unchecked")
@Transactional
public class DaoSupportImplementation<T> implements DaoSupport<T> {

    @Resource
    private SessionFactory sessionFactory;

    private Class<T> ClassOne;

    public DaoSupportImplementation() {
        ParameterizedType pt = (ParameterizedType) this.getClass().getGenericSuperclass();
        this.ClassOne = (Class<T>) pt.getActualTypeArguments()[0];
    }

    protected Session getSession() {
            return sessionFactory.getCurrentSession();
    }

    public void insert(T entity) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        session.save(entity);
        session.flush();
        transaction.commit();
    }

    public void update(T entity) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        session.update(entity);
        session.flush();
        transaction.commit();
    }

    public void delete(Serializable id) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        Object obj = findById(id);
        if (obj != null) {
            session.delete(obj);
        }
        transaction.commit();
    }

    public void deleteByIds(Integer[] ids) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        session.createQuery(//
                "DELETE FROM " + ClassOne.getSimpleName() + " WHERE id IN (:ids)")//
                .setParameterList("ids", ids)//
                .executeUpdate();
        transaction.commit();
    }

    public T findById(Serializable id) {
        Session session = getSession();
        T entity;
        if (id == null) {
            entity = null;
        } else {
            entity = (T) session.get(ClassOne, id);
        }
        return entity;
    }

    public List<T> findByIds(Integer[] ids) {
        Session session = getSession();
        List<T> list;
        if (ids == null || ids.length == 0) {
            list = Collections.EMPTY_LIST;
        } else {
            list = session.createQuery(//
                    "FROM " + ClassOne.getSimpleName() + " WHERE id IN (:ids)")//
                    .setParameterList("ids", ids)//
                    .list();
        }
        return list;
    }

    public List<T> findAll() {
        Session session = getSession();
        List<T> list = session.createQuery(//
                "FROM " + ClassOne.getSimpleName())//
                .list();
        return list;
    }

    public boolean isFieldExisted(String field, String value) {
        Session session = getSession();
        List<T> list = session.createQuery(//
                "FROM " + ClassOne.getSimpleName() + " WHERE "+field+" = (:field)")//
                .setParameter("field", value)//
                .list();
        return 	list.size()!=0;
    }
}
