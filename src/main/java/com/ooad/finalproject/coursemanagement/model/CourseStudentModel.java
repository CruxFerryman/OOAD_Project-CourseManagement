package com.ooad.finalproject.coursemanagement.model;

/**
 * Created by liuhe on 20/12/2016.
 */

import javax.persistence.*;

@Entity
@Table(name = "courseStudentTable", schema = "coursemanagement", catalog = "")
@IdClass(CourseStudentPriKeyModel.class)
public class CourseStudentModel {
    private int courseId;
    private int sid;
    private double grade;


    @Id
    @Column(name = "courseId", nullable = false)
    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    @Id
    @Column(name = "sid", nullable = false)
    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    @Basic
    @Column(name = "grade", precision = 1)
    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CourseStudentModel courseStudentModel = (CourseStudentModel) o;

        if (courseId != courseStudentModel.courseId) return false;
        if (sid != courseStudentModel.sid) return false;
        if (Double.compare(courseStudentModel.grade, grade) != 0) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = courseId;
        result = 31 * result + sid;
        temp = Double.doubleToLongBits(grade);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
