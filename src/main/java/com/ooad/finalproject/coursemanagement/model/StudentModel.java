package com.ooad.finalproject.coursemanagement.model;

import javax.persistence.*;

/**
 * Created by liuhe on 21/12/2016.
 */
@Entity
@Table(name = "studentTable", schema = "coursemanagement", catalog = "")
public class StudentModel {
    private int id;
    private String sid;
    private String password;
    private String name;
    private byte gender;
    private String notes;
    private int cid;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "sid", nullable = false, length = 63)
    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 31)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 63)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "gender", nullable = false)
    public byte getGender() {
        return gender;
    }

    public void setGender(byte gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "notes", nullable = true, length = 255)
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Basic
    @Column(name = "cid", nullable = false)
    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StudentModel studentModel = (StudentModel) o;

        if (id != studentModel.id) {
            return false;
        }
        if (gender != studentModel.gender) {
            return false;
        }
        if (cid != studentModel.cid) {
            return false;
        }
        if (sid != null ? !sid.equals(studentModel.sid) : studentModel.sid != null) {
            return false;
        }
        if (password != null ? !password.equals(studentModel.password) : studentModel.password != null) {
            return false;
        }
        if (name != null ? !name.equals(studentModel.name) : studentModel.name != null) {
            return false;
        }
        if (notes != null ? !notes.equals(studentModel.notes) : studentModel.notes != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (sid != null ? sid.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) gender;
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        result = 31 * result + cid;
        return result;
    }
}
