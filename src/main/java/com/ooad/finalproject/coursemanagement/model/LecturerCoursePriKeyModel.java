package com.ooad.finalproject.coursemanagement.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by liuhe on 20/12/2016.
 */
public class LecturerCoursePriKeyModel implements Serializable{
    private int staffId;
    private int courseId;

    @Id
    @Column(name = "staffId", nullable = false)
    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    @Id
    @Column(name = "courseId", nullable = false)
    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LecturerCoursePriKeyModel lecturerCoursePriKeyModel = (LecturerCoursePriKeyModel) o;

        if (staffId != lecturerCoursePriKeyModel.staffId) {
            return false;
        }
        if (courseId != lecturerCoursePriKeyModel.courseId) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = staffId;
        result = 31 * result + courseId;
        return result;
    }
}
