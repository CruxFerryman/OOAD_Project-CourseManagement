package com.ooad.finalproject.coursemanagement.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by liuhe on 20/12/2016.
 */
public class CourseStudentPriKeyModel implements Serializable {
    private int courseId;
    private int sid;

    @Column(name = "courseId", nullable = false)
    @Id
    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    @Column(name = "sid", nullable = false)
    @Id
    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CourseStudentPriKeyModel courseStudentPriKeyModel = (CourseStudentPriKeyModel) o;

        if (courseId != courseStudentPriKeyModel.courseId) {
            return false;
        }
        if (sid != courseStudentPriKeyModel.sid) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = courseId;
        result = 31 * result + sid;
        return result;
    }
}
