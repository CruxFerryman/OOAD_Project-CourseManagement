package com.ooad.finalproject.coursemanagement.model;

import javax.persistence.*;

/**
 * Created by liuhe on 20/12/2016.
 */
@Entity
@Table(name = "lecturerTable", schema = "coursemanagement", catalog = "")
public class LecturerModel {
    private int id;
    private String staffId;
    private String password;
    private String name;
    private byte gender;
    private String notes;
    private byte isAdmin;
    private int deptId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "staffId", nullable = false, length = 63)
    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 31)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 63)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "gender", nullable = false)
    public byte getGender() {
        return gender;
    }

    public void setGender(byte gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "notes", nullable = true, length = 255)
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Basic
    @Column(name = "isAdmin", nullable = false)
    public byte getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(byte isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Basic
    @Column(name = "deptId", nullable = false)
    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LecturerModel lecturerModel = (LecturerModel) o;

        if (id != lecturerModel.id) return false;
        if (gender != lecturerModel.gender) return false;
        if (isAdmin != lecturerModel.isAdmin) return false;
        if (deptId != lecturerModel.deptId) return false;
        if (staffId != null ? !staffId.equals(lecturerModel.staffId) : lecturerModel.staffId != null) {
            return false;
        }
        if (password != null ? !password.equals(lecturerModel.password) : lecturerModel.password != null) {
            return false;
        }
        if (name != null ? !name.equals(lecturerModel.name) : lecturerModel.name != null) {
            return false;
        }
        if (notes != null ? !notes.equals(lecturerModel.notes) : lecturerModel.notes != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (staffId != null ? staffId.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) gender;
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        result = 31 * result + (int) isAdmin;
        result = 31 * result + deptId;
        return result;
    }
}
