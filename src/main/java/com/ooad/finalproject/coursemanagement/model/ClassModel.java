package com.ooad.finalproject.coursemanagement.model;

/**
 * Created by liuhe on 20/12/2016.
 */

import javax.persistence.*;

@Entity
@Table(name = "classTable", schema = "coursemanagement", catalog = "")
public class ClassModel {
    private int id;
    private String name;
    private int deptId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 63)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "deptId", nullable = false)
    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClassModel classModel = (ClassModel) o;
        if (id != classModel.id) {
            return false;
        }
        if (deptId != classModel.deptId) {
            return false;
        }
        if (name != null ? !name.equals(classModel.name) : classModel.name != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + deptId;
        return result;
    }
}
