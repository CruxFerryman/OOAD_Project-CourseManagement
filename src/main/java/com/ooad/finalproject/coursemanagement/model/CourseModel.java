package com.ooad.finalproject.coursemanagement.model;

import javax.persistence.*;

/**
 * Created by liuhe on 20/12/2016.
 */
@Entity
@Table(name = "courseTable", schema = "coursemanagement", catalog = "")
public class CourseModel {
    private int id;
    private String courseId;
    private String name;
    private byte weekStart;
    private byte weekEnd;
    private double credit;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "courseId", nullable = false, length = 63)
    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 63)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "weekStart", nullable = false)
    public byte getWeekStart() {
        return weekStart;
    }

    public void setWeekStart(byte weekStart) {
        this.weekStart = weekStart;
    }

    @Basic
    @Column(name = "weekEnd", nullable = false)
    public byte getWeekEnd() {
        return weekEnd;
    }

    public void setWeekEnd(byte weekEnd) {
        this.weekEnd = weekEnd;
    }

    @Basic
    @Column(name = "credit", nullable = false, precision = 1)
    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CourseModel courseModel = (CourseModel) o;

        if (id != courseModel.id) {
            return false;
        }
        if (weekStart != courseModel.weekStart) {
            return false;
        }
        if (weekEnd != courseModel.weekEnd) {
            return false;
        }
        if (Double.compare(courseModel.credit, credit) != 0) {
            return false;
        }
        if (courseId != null ? !courseId.equals(courseModel.courseId) : courseModel.courseId != null) {
            return false;
        }
        if (name != null ? !name.equals(courseModel.name) : courseModel.name != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (courseId != null ? courseId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) weekStart;
        result = 31 * result + (int) weekEnd;
        temp = Double.doubleToLongBits(credit);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}


