package com.ooad.finalproject.coursemanagement.model;
/**
 * Created by liuhe on 20/12/2016.
 */

import javax.persistence.*;

@Entity
@Table(name = "courseLecturerTable", schema = "coursemanagement", catalog = "")
@IdClass(LecturerCoursePriKeyModel.class)
public class LecturerCourseModel {
    private int staffId;
    private int courseId;

    @Id
    @Column(name = "staffId", nullable = false)
    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    @Id
    @Column(name = "courseId", nullable = false)
    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LecturerCourseModel lecturerCourseModel = (LecturerCourseModel) o;

        if (staffId != lecturerCourseModel.staffId) {
            return false;
        }
        if (courseId != lecturerCourseModel.courseId) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = staffId;
        result = 31 * result + courseId;
        return result;
    }
}
