package com.ooad.finalproject.coursemanagement.action;

import com.ooad.finalproject.coursemanagement.model.LecturerModel;
import com.ooad.finalproject.coursemanagement.service.LecturerService;
import com.ooad.finalproject.coursemanagement.util.MD5Encoder;
import com.ooad.finalproject.coursemanagement.util.Message;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by liuhe on 21/12/2016.
 */
@RequestMapping("/Admin/lecturer")
@Controller
public class ManageLecturerAction {
    @Resource
    private LecturerService service;

    @ResponseBody
    @RequestMapping("/getAll")
    public Message getAll(){
        return new Message(1,"ok",service.findAllWithoutAdmin());
    }

    @ResponseBody
    @RequestMapping("/add")
    public Message add(@RequestParam String staffId, @RequestParam String password, @RequestParam String name, @RequestParam byte gender, @RequestParam int deptId, String notes){
        if (service.isFieldExisted("staffId",staffId))
            return new Message(-1,"已存在工号“"+staffId+"”");
        LecturerModel lecturer = new LecturerModel();
        lecturer.setStaffId(staffId);
        lecturer.setPassword(MD5Encoder.encode(password));
        lecturer.setName(name);
        lecturer.setGender(gender);
        lecturer.setDeptId(deptId);
        lecturer.setNotes(notes);
        lecturer.setIsAdmin((byte) 0);
        service.insert(lecturer);
        return new Message(1,"ok");
    }

    @ResponseBody
    @RequestMapping("/del")
    public Message del(@RequestParam("ids[]") Integer[] ids){
        service.deleteCascade(ids);
        return new Message(1,"ok");
    }

    @ResponseBody
    @RequestMapping("/upd")
    public Message upd(@RequestParam int id, String staffId, String password, String name, Byte gender, Integer deptId, String notes){
        LecturerModel lecturer = service.findById(id);
        if (!lecturer.getStaffId().equals(staffId)&&service.isFieldExisted("staffId",staffId))
            return new Message(-1,"已存在工号“"+staffId+"”");
        if (staffId!=null)
            lecturer.setStaffId(staffId);
        if (password!=null)
            lecturer.setPassword(MD5Encoder.encode(password));
        if (name!=null)
            lecturer.setName(name);
        if (gender!=null)
            lecturer.setGender(gender);
        if (deptId!=null)
            lecturer.setDeptId(deptId);
        if (notes!=null)
            lecturer.setNotes(notes);
        service.update(lecturer);
        return new Message(1,"ok");
    }
}
