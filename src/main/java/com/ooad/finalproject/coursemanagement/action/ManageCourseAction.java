package com.ooad.finalproject.coursemanagement.action;

import com.ooad.finalproject.coursemanagement.model.CourseModel;
import com.ooad.finalproject.coursemanagement.service.CourseService;
import com.ooad.finalproject.coursemanagement.util.Message;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by liuhe on 21/12/2016.
 */
@RequestMapping("/Admin/course")
@Controller
public class ManageCourseAction {

    @Resource
    private CourseService service;

    @ResponseBody
    @RequestMapping("/getAll")
    public Message getAll(){
        Message msg = new Message(1,"ok");
        msg.setData(service.findAll());
        return msg;
    }

    @ResponseBody
    @RequestMapping("/add")
    public Message add(@RequestParam String courseId, @RequestParam String name, @RequestParam Byte weekStart, @RequestParam Byte weekEnd, @RequestParam Double credit){
        if (service.isFieldExisted("courseId",courseId))
            return new Message(-1,"已存在课程号“"+courseId+"”");
        if (service.isFieldExisted("name",name))
            return new Message(-1,"已存在课程“"+name+"”");
        CourseModel course = new CourseModel();
        course.setCourseId(courseId);
        course.setName(name);
        course.setWeekStart(weekStart);
        course.setWeekEnd(weekEnd);
        course.setCredit(credit);
        service.insert(course);
        return new Message(1,"ok");
    }

    @ResponseBody
    @RequestMapping("/del")
    public Message del(@RequestParam("ids[]") Integer[] ids){
        service.deleteByIds(ids);
        return new Message(1,"ok");
    }

    @ResponseBody
    @RequestMapping("/upd")
    public Message upd(@RequestParam int id, String courseId, String name, Byte weekStart, Byte weekEnd, Double credit) {
        CourseModel course = service.findById(id);
        if (courseId != null && !course.getCourseId().equals(courseId) && service.isFieldExisted("courseId", courseId))
            return new Message(-1, "已存在课程号“" + name + "”");
        if (!course.getName().equals(name) && service.isFieldExisted("name", name))
            return new Message(-1, "已存在课程“" + name + "”");
        if (courseId != null) course.setCourseId(courseId);
        if (name != null) course.setName(name);
        if (weekStart != null) course.setWeekStart(weekStart);
        if (weekEnd != null) course.setWeekEnd(weekEnd);
        if (credit != null) course.setCredit(credit);
        service.update(course);
        return new Message(1, "ok");
    }
}
