package com.ooad.finalproject.coursemanagement.action;


import com.ooad.finalproject.coursemanagement.model.StudentModel;
import com.ooad.finalproject.coursemanagement.service.StudentService;
import com.ooad.finalproject.coursemanagement.util.MD5Encoder;
import com.ooad.finalproject.coursemanagement.util.Message;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by liuhe on 21/12/2016.
 */
@RequestMapping("/Admin/student")
@Controller
public class ManageStudentAction {
    @Resource
    private StudentService service;

    @ResponseBody
    @RequestMapping("/getAll")
    public Message getAll(){
        return new Message(1,"ok",service.findAll());
    }

    @ResponseBody
    @RequestMapping("/add")
    public Message add(@RequestParam String sid, String password, @RequestParam String name, @RequestParam byte gender, @RequestParam int cid, String notes){
        if (service.isFieldExisted("sid",sid))
            return new Message(-1,"已存在学号“"+sid+"”");
        StudentModel student = new StudentModel();
        student.setSid(sid);
        student.setPassword(MD5Encoder.encode(password==null?"011235813":password));
        student.setName(name);
        student.setGender(gender);
        student.setCid(cid);
        student.setNotes(notes);
        service.insert(student);
        return new Message(1,"ok");
    }

    @ResponseBody
    @RequestMapping("/del")
    public Message del(@RequestParam("ids[]") Integer[] ids){
        service.deleteCascade(ids);
        return new Message(1,"ok");
    }

    @ResponseBody
    @RequestMapping("/upd")
    public Message upd(@RequestParam int id, String sid, String password, String name, Byte gender, Integer cid, String notes){
        StudentModel student = service.findById(id);
        if (!student.getSid().equals(sid)&&service.isFieldExisted("sid",sid))
            return new Message(-1,"已存在学号“"+sid+"”");
        if (sid!=null)
            student.setSid(sid);
        if (password!=null)
            student.setPassword(MD5Encoder.encode(password));
        if (name!=null)
            student.setName(name);
        if (gender!=null)
            student.setGender(gender);
        if (cid!=null)
            student.setCid(cid);
        if (notes!=null)
            student.setNotes(notes);
        service.update(student);
        return new Message(1,"ok");
    }
}
