package com.ooad.finalproject.coursemanagement.action;

import com.ooad.finalproject.coursemanagement.model.ClassModel;
import com.ooad.finalproject.coursemanagement.service.ClassService;
import com.ooad.finalproject.coursemanagement.util.Message;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by liuhe on 21/12/2016.
 */
@RequestMapping("/Admin/class")
@Controller
public class ManageClassAction {
    @Resource
    private ClassService service;

    @ResponseBody
    @RequestMapping("/getAll")
    public Message getAll(){
        return new Message(1,"ok",service.findAll());
    }

    @ResponseBody
    @RequestMapping("/add")
    public Message add(@RequestParam String name, @RequestParam int deptId){
        if (service.isFieldExisted("name",name))
            return new Message(-1,"已存在“"+name+"”");
        ClassModel classModel = new ClassModel();
        classModel.setName(name);
        classModel.setDeptId(deptId);
        service.insert(classModel);
        return new Message(1,"ok");
    }

    @ResponseBody
    @RequestMapping("/del")
    public Message del(@RequestParam("ids[]") Integer[] ids){
        service.deleteByIds(ids);
        return new Message(1,"ok");
    }

    @ResponseBody
    @RequestMapping("/upd")
    public Message upd(@RequestParam int id,@RequestParam String name,@RequestParam int deptId){
        ClassModel classModel = service.findById(id);
        if (!classModel.getName().equals(name)&&service.isFieldExisted("name",name))
            return new Message(-1,"已存在“"+name+"”");
        classModel.setName(name);
        classModel.setDeptId(deptId);
        service.update(classModel);
        return new Message(1,"ok");
    }
}
