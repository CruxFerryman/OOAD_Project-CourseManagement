package com.ooad.finalproject.coursemanagement.action;


import com.ooad.finalproject.coursemanagement.service.CourseService;
import com.ooad.finalproject.coursemanagement.service.CourseStudentService;
import com.ooad.finalproject.coursemanagement.service.LecturerCourseService;
import com.ooad.finalproject.coursemanagement.util.Message;
import com.ooad.finalproject.coursemanagement.util.Token;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by liuhe on 21/12/2016.
 */
@RequestMapping("/Student")
@Controller
public class StudentAction {
    @Resource
    private CourseService courseService;
    @Resource
    private LecturerCourseService lcService;
    @Resource
    private CourseStudentService csService;

    @ResponseBody
    @RequestMapping("/course/getAll")
    public Message getAllCourse(){
        return new Message(1,"ok",lcService.getCoursesWithLecturers());
    }

    @ResponseBody
    @RequestMapping("/course/selectCourses")
    public Message selectCourses(@RequestParam("ids[]") Integer[] ids, @RequestHeader("Authorization") String token){
        int userID = new Token(token).getId();
        return csService.selectCourses(userID, ids);
    }

    @ResponseBody
    @RequestMapping("/course/unselectCourses")
    public Message unselectCourses(@RequestParam("ids[]") Integer[] ids, @RequestHeader("Authorization") String token){
        int userID = new Token(token).getId();
        return csService.unselectCourses(userID, ids);
    }

    @ResponseBody
    @RequestMapping("/course/selectedCourses")
    public Message selectCourses(@RequestHeader("Authorization") String token){
        int id = new Token(token).getId();
        return new Message(1,"ok",courseService.findCoursesBySid(id));
    }

    @ResponseBody
    @RequestMapping("/grade/queryGrade")
    public Message queryGrade(@RequestHeader("Authorization") String token){
        int id = new Token(token).getId();
        return new Message(1,"ok", csService.queryGrade(id));
    }
}
