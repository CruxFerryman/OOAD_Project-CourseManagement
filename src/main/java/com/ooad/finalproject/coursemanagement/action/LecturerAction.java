package com.ooad.finalproject.coursemanagement.action;

import com.ooad.finalproject.coursemanagement.service.*;
import com.ooad.finalproject.coursemanagement.util.Message;
import com.ooad.finalproject.coursemanagement.util.Token;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by liuhe on 21/12/2016.
 */
@RequestMapping("/Lecturer")
@Controller
public class LecturerAction {
    @Resource
    private CourseService courseService;
    @Resource
    private StudentService studentService;
    @Resource
    private LecturerCourseService lcService;
    @Resource
    private ClassService classService;
    @Resource
    private CourseStudentService csService;

    @ResponseBody
    @RequestMapping("/course/getAll")
    public Message getAllCourse(){
        return new Message(1,"ok",courseService.getAvailableCourses());
    }

    @ResponseBody
    @RequestMapping("/course/selectCourses")
    public Message selectCourses(@RequestParam("ids[]") Integer[] ids, @RequestHeader("Authorization") String token){
        int userID = new Token(token).getId();
        return lcService.selectCourses(userID, ids);
    }

    @ResponseBody
    @RequestMapping("/course/unselectCourses")
    public Message unselectCourses(@RequestParam("ids[]") Integer[] ids, @RequestHeader("Authorization") String token){
        int userID = new Token(token).getId();
        return lcService.unselectCourses(userID, ids);
    }

    @ResponseBody
    @RequestMapping("/course/selectedCourses")
    public Message selectCourses(@RequestHeader("Authorization") String token){
        int id = new Token(token).getId();
        return new Message(1,"ok",courseService.findCoursesByStaffId(id));
    }

    @ResponseBody
    @RequestMapping("/grade/findStudentsByCourseId")
    public Message findStudentsByCourseId(@RequestParam int id){
        return new Message(1,"ok",studentService.findStudentsByCourseId(id));
    }

    @ResponseBody
    @RequestMapping("/class/getAll")
    public Message getAll(){
        return new Message(1,"ok",classService.findAll());
    }

    @ResponseBody
    @RequestMapping("/class/getGradeByCourseId")
    public Message getGradeByCourseId(@RequestParam int id){
        return new Message(1,"ok",csService.getGradeByCourseId(id));
    }

    @ResponseBody
    @RequestMapping("/class/getGradeBySid")
    public Message getGradeBySid(@RequestParam int id){
        return new Message(1,"ok",csService.getGradeBySid(id));
    }

    @ResponseBody
    @RequestMapping("/grade/recordGrade")
    public Message recordGrade(@RequestParam int courseId, @RequestParam int sid, @RequestParam double grade, @RequestHeader("Authorization") String token){
        return csService.recordGrade(new Token(token).getId(),courseId,sid,grade);
    }
}
