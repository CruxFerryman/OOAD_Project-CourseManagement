package com.ooad.finalproject.coursemanagement.action;

import com.ooad.finalproject.coursemanagement.model.LecturerModel;
import com.ooad.finalproject.coursemanagement.model.StudentModel;
import com.ooad.finalproject.coursemanagement.service.LecturerService;
import com.ooad.finalproject.coursemanagement.service.StudentService;
import com.ooad.finalproject.coursemanagement.util.Auth;
import com.ooad.finalproject.coursemanagement.util.MD5Encoder;
import com.ooad.finalproject.coursemanagement.util.Message;
import com.ooad.finalproject.coursemanagement.util.Token;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by liuhe on 21/12/2016.
 */
@RequestMapping("/Access")
@Controller
public class AccessAction {

    @Resource
    private StudentService sService;
    @Resource
    private LecturerService lService;

    @ResponseBody
    @RequestMapping("signin")
    public Message signin(@RequestParam String name, @RequestParam String password, @RequestParam String role) {
        Message message = new Message(1, "ok");
        Map<String, Object> claims = new HashMap<String, Object>();
        if (role.equals("student")) {//学生账号
            StudentModel student = sService.login(name, password);
            if (student == null)
                return new Message(-1, "用户名或密码错误");
            claims.put("id", student.getId());
            claims.put("name", student.getName());
            claims.put("role", "student");
        } else if (role.equals("lecturer")) {//教师账号
            LecturerModel lecturer = lService.login(name, password);
            if (lecturer == null)
                return new Message(-1, "用户名或密码错误");
            claims.put("id", lecturer.getId());
            claims.put("name", lecturer.getName());
            claims.put("role", "lecturer");
        } else if (role.equals("admin")) {//管理员账号
            LecturerModel lecturer = lService.login(name, password);
            if (lecturer == null)
                return new Message(-1, "用户名或密码错误");
            if (lecturer.getIsAdmin() == 0)
                return new Message(-2, "非管理员账号");
            claims.put("id", lecturer.getId());
            claims.put("name", lecturer.getName());
            claims.put("role", "admin");
        }
        String s = Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, Auth.key).setExpiration(new Date(System.currentTimeMillis() + Auth.expire)).compact();
        claims.put("token", s);
        claims.remove("exp");
        message.setData(claims);
        return message;
    }

    @ResponseBody
    @RequestMapping("userInfo")
    public Message userInfo(@RequestHeader("Authorization") String token) {
        Token t = new Token(token);
        int id = t.getId();
        String role = t.getRole();
        if (role.equals("student"))
            return new Message(1, "ok", sService.findById(id));
        else if (role.equals("lecturer") || role.equals("admin"))
            return new Message(1, "ok", lService.findById(id));
        else
            return new Message(-1, "用户身份不符");
    }

    @ResponseBody
    @RequestMapping("changePwd")
    public Message changePwd(@RequestParam String oldPwd, @RequestParam String newPwd, @RequestHeader("Authorization") String token) {
        Token t = new Token(token);
        int id = t.getId();
        String role = t.getRole();
        if (role.equals("student")) {
            StudentModel student = sService.findById(id);
            if (!student.getPassword().equals(MD5Encoder.encode(oldPwd)))
                return new Message(-2, "原密码错误");
            student.setPassword(MD5Encoder.encode(newPwd));
            sService.update(student);
        } else if (role.equals("lecturer") || role.equals("admin")) {
            LecturerModel lecturer = lService.findById(id);
            if (!lecturer.getPassword().equals(MD5Encoder.encode(oldPwd)))
                return new Message(-2, "原密码错误");
            lecturer.setPassword(MD5Encoder.encode(newPwd));
            lService.update(lecturer);
        }
        return new Message(1, "ok");
    }
}