package com.ooad.finalproject.coursemanagement.auth.interceptor;

import com.ooad.finalproject.coursemanagement.util.Token;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by liuhe on 21/12/2016.
 */
public class Access extends CrossDomain {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String path = request.getContextPath();
        String uri = request.getRequestURI();
        if (!uri.startsWith(path+"/Access")) {
            String auth = request.getHeader("Authorization");
            if (auth==null){
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                        "Authentication Failed: Require Authorization");
                return false;
            }
            Token token = new Token(auth);
            if (token.getErr()==Token.ExpiredJwtError){
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                        "Authentication Failed: 认证过期");
                return false;
            }
            if (token.getErr()==Token.SignatureError){
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                        "Authentication Failed: 非法认证");
                return false;
            }
            boolean isAdmin = !token.getRole().equals("admin")&&uri.startsWith(path+"/Admin");
            boolean isTeacher = !token.getRole().equals("lecturer")&&uri.startsWith(path+"/Lecturer");
            boolean isStudent = !token.getRole().equals("student")&&uri.startsWith(path+"/Student");
            if (isAdmin||isTeacher||isStudent) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                        "Authentication Failed: 无权访问");
                return false;
            }
        }
        return true;
    }
}
