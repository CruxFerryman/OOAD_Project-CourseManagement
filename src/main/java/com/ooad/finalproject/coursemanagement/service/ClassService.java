package com.ooad.finalproject.coursemanagement.service;

import com.ooad.finalproject.coursemanagement.dao.DaoSupportImplementation;
import com.ooad.finalproject.coursemanagement.model.ClassModel;
import org.springframework.stereotype.Component;

/**
 * Created by liuhe on 21/12/2016.
 */
@Component
public class ClassService extends DaoSupportImplementation<ClassModel> {
}
