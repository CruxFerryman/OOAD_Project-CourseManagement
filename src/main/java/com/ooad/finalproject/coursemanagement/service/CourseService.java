package com.ooad.finalproject.coursemanagement.service;

import com.ooad.finalproject.coursemanagement.common.map.LecturerCommon;
import com.ooad.finalproject.coursemanagement.dao.DaoSupportImplementation;
import com.ooad.finalproject.coursemanagement.model.CourseModel;
import com.ooad.finalproject.coursemanagement.model.LecturerCourseModel;
import com.ooad.finalproject.coursemanagement.model.LecturerModel;
import org.hibernate.Query;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liuhe on 21/12/2016.
 */
@Component
public class CourseService extends DaoSupportImplementation<CourseModel> {

    public List getAvailableCourses(){
        Query query = getSession().createQuery("FROM CourseModel WHERE id not in (SELECT distinct courseId FROM LecturerCourseModel)");
        List list = query.list();
        return list;
    }

    public List findCoursesByStaffId(int id){
        Query query = getSession().createQuery("FROM CourseModel WHERE id in (SELECT courseId FROM LecturerCourseModel WHERE staffId = (:staffId))");
        List list = query.setParameter("staffId",id).list();
        return list;
    }

    public Map<String,List> findCoursesBySid(int id){
        Map<String,List> data = new HashMap<String, List>();
        Query query = getSession().createQuery("FROM CourseModel WHERE id in (SELECT courseId FROM CourseStudentModel WHERE sid = (:sid))");
        List<CourseModel> courses = query.setParameter("sid",id).list();
        data.put("courses",courses);
        Integer[] courseIds = new Integer[courses.size()];
        for (int i=0;i<courses.size();i++)
            courseIds[i] = courses.get(i).getId();
        List<LecturerModel> lecturers;
        List<LecturerCourseModel> lc;
        if (courseIds.length==0){
            lecturers = new ArrayList<LecturerModel>();
            lc = new ArrayList<LecturerCourseModel>();
        }
        else{
            lecturers = getSession().createQuery("FROM LecturerModel WHERE id in (SELECT DISTINCT staffId FROM LecturerCourseModel WHERE courseId in (:courseId))")
                        .setParameterList("courseId",courseIds).list();
            lc = getSession().createQuery("FROM LecturerCourseModel WHERE courseId in (:courseId)").setParameterList("courseId",courseIds).list();
        }

        data.put("lecturers", LecturerCommon.lecturerList(lecturers));
        data.put("lc", lc);
        return data;
    }

}
