package com.ooad.finalproject.coursemanagement.service;

import com.ooad.finalproject.coursemanagement.common.map.LecturerCommon;
import com.ooad.finalproject.coursemanagement.dao.DaoSupportImplementation;
import com.ooad.finalproject.coursemanagement.model.CourseModel;
import com.ooad.finalproject.coursemanagement.model.LecturerCourseModel;
import com.ooad.finalproject.coursemanagement.model.LecturerModel;
import com.ooad.finalproject.coursemanagement.util.Message;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liuhe on 21/12/2016.
 */
@Component
public class LecturerCourseService extends DaoSupportImplementation<LecturerCourseModel> {

    public Message selectCourses(int userID, Integer[] ids){
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        if (session.createQuery("FROM LecturerCourseModel WHERE staffId = (:staffId) and courseId in (:courseId)")
                .setParameter("staffId",userID)
                .setParameterList("courseId",ids).list().size()>0) {
            return new Message(-1, "该课程可能已选过，请重试。");
        }
        for (int i :ids){
            LecturerCourseModel lecturerCourseModel = new LecturerCourseModel();
            lecturerCourseModel.setStaffId(userID);
            lecturerCourseModel.setCourseId(i);
            session.save(lecturerCourseModel);
        }
        session.flush();
        tx.commit();

        return new Message(1,"ok");
    }

    public Message unselectCourses(int userID, Integer[] ids) {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        int rows = session.createQuery("DELETE FROM LecturerCourseModel WHERE staffId = (:staffId) and courseId in (:courseId)")
                .setParameter("staffId", userID)
                .setParameterList("courseId", ids).executeUpdate();
        tx.commit();
        return rows > 0 ? new Message(1, "操作已成功完成。") : new Message(-1, "退课失败，请联系教学工作部。");
    }

    public Map<String,List> getCoursesWithLecturers(){
        Map<String,List> data = new HashMap<String, List>();
        List<CourseModel> courseModels = getSession().createQuery("FROM CourseModel WHERE id in (SELECT DISTINCT courseId FROM LecturerCourseModel)").list();
        data.put("courses",courseModels);
        Integer[] courseIds = new Integer[courseModels.size()];
        for (int i=0;i<courseModels.size();i++)
            courseIds[i] = courseModels.get(i).getId();
        List<LecturerModel> lecturers;
        if (courseIds.length==0) lecturers = new ArrayList<LecturerModel>();
        else lecturers = getSession().createQuery("FROM LecturerModel WHERE id in (SELECT DISTINCT staffId FROM LecturerCourseModel WHERE courseId in (:courseId))")
                .setParameterList("courseId",courseIds).list();
        data.put("lecturers", LecturerCommon.lecturerList(lecturers));
        data.put("lc",findAll());

        return data;
    }

}
