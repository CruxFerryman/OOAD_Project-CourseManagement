package com.ooad.finalproject.coursemanagement.service;

import com.ooad.finalproject.coursemanagement.dao.DaoSupportImplementation;
import com.ooad.finalproject.coursemanagement.model.CourseStudentModel;
import com.ooad.finalproject.coursemanagement.util.Message;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liuhe on 21/12/2016.
 */
@Component
public class CourseStudentService extends DaoSupportImplementation<CourseStudentModel> {

    public Message selectCourses(int userID, Integer[] ids) {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        if (session.createQuery("FROM CourseStudentModel WHERE sid = (:sid) and courseId in (:courseId)")
                .setParameter("sid",userID)
                .setParameterList("courseId",ids).list().size()>0) {
            return new Message(-1, "所选课程可能有重复，请检查后重选。如有问题请联系教学工作部。");
        }
        for (int i :ids){
            CourseStudentModel courseStudentModel = new CourseStudentModel();
            courseStudentModel.setSid(userID);
            courseStudentModel.setCourseId(i);
            session.save(courseStudentModel);
        }
        session.flush();
        tx.commit();

        return new Message(1,"ok");
    }

    public Message unselectCourses(int userID, Integer[] ids) {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        int rows = session.createQuery("DELETE FROM CourseStudentModel WHERE sid = (:sid) and courseId in (:courseId)")
                .setParameter("sid", userID)
                .setParameterList("courseId", ids).executeUpdate();
        tx.commit();
        return rows > 0 ? new Message(1, "ok") : new Message(-1, "退课失败……");
    }

    public List getGradeByCourseId(int id) {
        return getSession().createQuery("FROM CourseStudentModel WHERE courseId = (:courseId)")
                .setParameter("courseId",id).list();
    }

    public List getGradeBySid(int id) {
        return getSession().createQuery("FROM CourseStudentModel WHERE sid = (:sid)")
                .setParameter("sid",id).list();
    }

    public Message recordGrade(int staffId, int courseId, int sid, double grade){
        Session session = getSession();
        if(session.createQuery("FROM LecturerCourseModel WHERE staffId = (:staffId) and courseId = (:courseId)")
                .setParameter("staffId",staffId)
                .setParameter("courseId",courseId).list().size()==0)
            return new Message(-1,"您非此课程的授课教师，无法录入成绩");
        Transaction tx = session.beginTransaction();
        int row = session.createQuery("UPDATE CourseStudentModel SET grade = (:grade) WHERE courseId = (:courseId) and sid = (:sid)")
                .setParameter("grade",grade)
                .setParameter("courseId",courseId)
                .setParameter("sid",sid)
                .executeUpdate();
        tx.commit();
        return row>0 ? new Message(1,"ok") : new Message(-2,"未知错误，录入成绩失败，请咨询教学工作部。");
    }

    public Map<String,List> queryGrade(int sid){
        Map<String,List> data = new HashMap<String, List>();
        Session session = getSession();
        List grades = session.createQuery("FROM CourseStudentModel WHERE sid = (:sid) and grade != 0")
                .setParameter("sid",sid).list();
        Integer[] courseIds = new Integer[grades.size()];
        for (int i=0 ; i<grades.size() ; i++){
            CourseStudentModel courseStudentModel = (CourseStudentModel) grades.get(i);
            courseIds[i] = courseStudentModel.getCourseId();
        }
        List courses;
        if (courseIds.length==0) courses = new ArrayList();
        else courses = session.createQuery("FROM CourseModel WHERE id in (:courseId)").setParameterList("courseId",courseIds).list();
        data.put("grades", grades);
        data.put("courses", courses);
        return data;
    }

}
