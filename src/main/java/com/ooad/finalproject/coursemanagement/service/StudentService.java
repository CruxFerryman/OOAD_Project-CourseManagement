package com.ooad.finalproject.coursemanagement.service;

import com.ooad.finalproject.coursemanagement.dao.DaoSupportImplementation;
import com.ooad.finalproject.coursemanagement.model.StudentModel;
import com.ooad.finalproject.coursemanagement.util.MD5Encoder;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liuhe on 21/12/2016.
 */
@Component
public class StudentService extends DaoSupportImplementation<StudentModel> {

    @Resource
    private CourseService courseService;

    public StudentModel login(String sid, String password){
        List students = getSession().createQuery("FROM StudentModel WHERE sid = (:sid)")
                .setParameter("sid",sid).list();
        StudentModel studentModel = students.size()==0?null: (StudentModel) students.get(0);
        if (studentModel!=null){
            return studentModel.getPassword().equals(MD5Encoder.encode(password)) ? studentModel:null;
        }
        else{
            return null;
        }
    }

    public void deleteCascade(Integer[] ids){
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        session.createQuery("DELETE FROM CourseStudentModel WHERE sid in (:ids)")
                .setParameterList("ids", ids).executeUpdate();
        session.createQuery(
                "DELETE FROM CourseStudentModel WHERE sid IN (:ids)")
                .setParameterList("ids", ids).executeUpdate();
        tx.commit();
    }

    public Map<String,Object> findStudentsByCourseId(int id){
        Map<String,Object> data = new HashMap<String, Object>();
//        System.out.println(id);
//        List sidStudentlist = getSession().createQuery("FROM StudentModel WHERE sid in (SELECT sid FROM CourseStudentModel WHERE courseId in (SELECT id FROM CourseModel WHERE id = (:courseId)))")
//                .setParameter("courseId",id).list();
//        System.out.println(sidStudentlist);
        List students = getSession().createQuery("FROM StudentModel WHERE id in (SELECT sid FROM CourseStudentModel WHERE courseId  = (:courseId))")
                .setParameter("courseId",id).list();
        data.put("course", courseService.findById(id));
        data.put("students", students);
        System.out.println(students);
        return data;
    }
}
