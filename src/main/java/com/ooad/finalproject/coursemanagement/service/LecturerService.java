package com.ooad.finalproject.coursemanagement.service;

import com.ooad.finalproject.coursemanagement.dao.DaoSupportImplementation;
import com.ooad.finalproject.coursemanagement.model.LecturerModel;
import com.ooad.finalproject.coursemanagement.util.MD5Encoder;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by liuhe on 21/12/2016.
 */
@Component
public class LecturerService extends DaoSupportImplementation<LecturerModel> {

    public LecturerModel login(String staffId, String password){
        List lecturers = getSession().createQuery("FROM LecturerModel WHERE staffId = (:staffId)")
                .setParameter("staffId", staffId).list();
        LecturerModel lecturerModel = lecturers.size()==0?null: (LecturerModel) lecturers.get(0);
        if (lecturerModel != null) {
            return lecturerModel.getPassword().equals(MD5Encoder.encode(password)) ? lecturerModel : null;
        } else {
            return null;
        }
    }

    public void deleteCascade(Integer[] ids){
        Session session = getSession();
        Transaction tx = getSession().beginTransaction();
        session.createQuery("DELETE FROM LecturerCourseModel WHERE staffId IN (:ids)")
                .setParameterList("ids",ids).executeUpdate();
        session.createQuery(
                "DELETE FROM LecturerCourseModel WHERE staffId IN (:ids)")
                .setParameterList("ids", ids).executeUpdate();
        tx.commit();

    }

    public List findAllWithoutAdmin(){
        return getSession().createQuery("FROM LecturerModel WHERE isAdmin != 1").list();
    }

}
