package com.ooad.finalproject.coursemanagement.common.map;

import com.ooad.finalproject.coursemanagement.model.LecturerModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liuhe on 21/12/2016.
 */
public class LecturerCommon {

    public static List<Map<String, Object>> lecturerList(List<LecturerModel> lecturers) {
        List<Map<String, Object>> lecturerList = new ArrayList<Map<String, Object>>();
        for (LecturerModel l:lecturers) {
            Map<String, Object> lecturerMap = new HashMap<String, Object>();
            lecturerMap.put("id", l.getId());
            lecturerMap.put("name", l.getName());
            lecturerList.add(lecturerMap);
        }
        return lecturerList;
    }
}
